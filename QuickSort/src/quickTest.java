import org.junit.Test;
import org.junit.Assert;


public class quickTest {
	@Test
	public void igualdad()
	{
		int prueba1[] = {1,2,3,4,5};
		int prueba2[] = {3,1,2,4,5};
		int conte[] = new int [5];
		Ordenador or = new Ordenador();
		
		conte = or.ordenarQuickSort(prueba2);
		Assert.assertArrayEquals(conte, prueba1);
	}
	
	@Test
	public void desigualdad()
	{
		int prueba1[] = {1,2,3,4,6};
		int prueba2[] = {3,1,2,4,5};
		int conte[] = new int [5];
		Ordenador or = new Ordenador();
		
		conte = or.ordenarQuickSort(prueba2);
		Assert.assertNotEquals(conte, prueba1);
	}
	
	
}
